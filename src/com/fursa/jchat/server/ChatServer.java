package com.fursa.jchat.server;

import com.fursa.jchat.network.TCPConnection;
import com.fursa.jchat.network.TCPConnectionListener;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.List;

public class ChatServer implements TCPConnectionListener {

    public static void main(String[] args) {
        new ChatServer();
    }

    private final List<TCPConnection> tcpConnectionList = new ArrayList<>();

    private ChatServer() {
        System.out.println("[+] Server started...");

        try(ServerSocket socket = new ServerSocket(8189)) {
            while (true) {
                try {
                    new TCPConnection(this, socket.accept());
                } catch (IOException ex) {
                    System.out.println("[!] TCP connection error: " + ex.getLocalizedMessage());
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public synchronized void onConnectionReady(TCPConnection connection) {
        tcpConnectionList.add(connection);
        sentToAllClients("[+]Client " + connection.toString() + " connected...");
    }

    @Override
    public synchronized void onReceiveMessage(TCPConnection connection, String message) {
        sentToAllClients(message);
    }

    @Override
    public synchronized void onDisconnect(TCPConnection connection) {
        tcpConnectionList.remove(connection);
        sentToAllClients("[-] Client " + connection.toString() + " disconneced...");
    }

    @Override
    public synchronized void onError(TCPConnection connection, Exception e) {
        System.out.println("[!]TCP connection error: " + e);
    }

    private void sentToAllClients(String message) {
        System.out.println(" [->] sended: " + message);
        final int listSize = tcpConnectionList.size();

        for (int i = 0; i < listSize; i++) {
            TCPConnection connection = tcpConnectionList.get(i);
            connection.sendMessage(message);
        }
    }
}
