package com.fursa.jchat.network;

public interface TCPConnectionListener {

    void onConnectionReady(TCPConnection connection);

    void onReceiveMessage(TCPConnection connection, String message);

    void onDisconnect(TCPConnection connection);

    void onError(TCPConnection connection, Exception e);

}
