package com.fursa.jchat.network;

import java.io.*;
import java.net.Socket;
import java.nio.charset.Charset;

public class TCPConnection {

    private static final String DEFAULT_CHARSET = "UTF-8";

    private final Socket socket;
    private final Thread receiveThread;
    private final BufferedReader in;
    private final BufferedWriter out;
    private TCPConnectionListener eventListener;

    public TCPConnection(TCPConnectionListener listener, String ipAddr, int port) throws IOException {
        this(listener, new Socket(ipAddr, port));
    }

    public TCPConnection(TCPConnectionListener listener, Socket socket) throws IOException {
        this.socket = socket;
        this.eventListener = listener;

        in = new BufferedReader(new InputStreamReader(socket.getInputStream(), Charset.forName(DEFAULT_CHARSET)));
        out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), Charset.forName(DEFAULT_CHARSET)));

        receiveThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    eventListener.onConnectionReady(TCPConnection.this);
                    while (!receiveThread.isInterrupted()) {
                        eventListener.onReceiveMessage(TCPConnection.this, in.readLine());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    eventListener.onDisconnect(TCPConnection.this);
                }
            }
        });

        receiveThread.start();
    }

    public synchronized void sendMessage(String messageToSend) {
        try {
            out.write(messageToSend + "\n");
            out.flush();
        } catch (IOException e) {
            eventListener.onError(TCPConnection.this, e);
            disconnect();
        }
    }

    public synchronized void disconnect() {
        receiveThread.interrupt();
        try {
            socket.close();
        } catch (IOException e) {
            eventListener.onError(TCPConnection.this, e);
        }
    }

    @Override
    public String toString() {
        return "TCPConnectionP: " + socket.getInetAddress() + ": " + socket.getPort();
    }
}
